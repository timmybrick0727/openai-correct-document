import React, { useState, useEffect } from "react";
import { Comment } from "react-loader-spinner";
import axios from "axios";
import { styles } from "./styles"; // 引入 styles
import { ReactComponent as SettingsIcon } from "./setting.svg";

// const SERVER_URL = process.env.REACT_APP_SERVER_URL || "http://localhost:5050";

function App() {
  const [file, setFile] = useState(null);
  const [text, setText] = useState("");
  const [textSummary, setTextSummary] = useState("");
  const [prompt, setPrompt] = useState(
    "1.拼寫檢查 2.語法檢查 3.標點符號必須為全形 4.檢查是否出現中文簡體、英文字詞全部大寫 5.檢查所有頁面所有文字及表格"
  );
  const [feature, setFeature] = useState("校正文稿");
  const [answer, setAnswer] = useState("");
  const [answerSummary, setAnswerSummary] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [isUploadLoading, setIsUploadLoading] = useState(false);
  const [isIntroVisible, setIsIntroVisible] = useState(true);
  const [isPasswordIncorrect, setIsPasswordIncorrect] = useState(false);
  const [selectedOption, setSelectedOption] = useState(
    "正式版 (含 API Key，登入後可直接使用)"
  );
  const [showfirstStep, setShowfirstStep] = useState(true);
  const [showEAPasswordBar, setShowEAPasswordBar] = useState(false);
  const [showLogoutMenu, setShowLogoutMenu] = useState(false);
  const [EAPassword, setEAPassword] = useState("");
  const [showAPIKeyModal, setShowAPIKeyModal] = useState(false);
  const [apiKey, setApiKey] = useState("");

  const [checkboxValues, setCheckboxValues] = useState({
    拼寫檢查: true,
    語法檢查: true,
    標點符號必須為全形: true,
    "檢查是否出現中文簡體、英文字詞全部大寫": true,
    檢查所有頁面所有文字及表格: true,
  });

  const [newCorrectionItem, setNewCorrectionItem] = useState("");
  const [addedItems, setAddedItems] = useState([]);

  const onFileChange = async (e) => {
    setIsUploadLoading(true);
    setFile(e.target.files[0]);
    const formData = new FormData();
    formData.append("file", e.target.files[0]);
    try {
      const response = await axios.post(`/upload`, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      if (feature === "校正文稿") {
        setText(response.data);
      } else {
        setTextSummary(response.data);
      }
    } catch (error) {
      console.error("Error uploading file:", error);
    }
    setIsUploadLoading(false);
  };

  const handleMouseOver = (e) => {
    e.currentTarget.style.transform = "scale(1.005)";
  };

  const handleMouseOut = (e) => {
    e.currentTarget.style.transform = "scale(1)";
  };

  const handlePromptChange = (e) => {
    setPrompt(e.target.value);
  };

  const handleFeatureChange = (e) => {
    setFeature(e.target.value);
  };

  const handleSubmit = async (task, data, api_key) => {
    setIsLoading(true);
    const response = await fetch("/get_answer", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ task, data, api_key }),
    });

    if (response.ok) {
      const result = await response.json();
      setAnswer(result.answer);
      setFeature("校正文稿");
    } else {
      alert("出現錯誤，請檢查Open API Key，或稍後再嘗試！");
    }
    setIsLoading(false);
  };

  const handleSubmitSummary = async (data, api_key) => {
    setIsLoading(true);
    const response = await fetch("/get_answer_summary", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ data, api_key }),
    });

    if (response.ok) {
      const result = await response.json();
      setAnswerSummary(result.answer);
      setFeature("摘要統整");
    } else {
      alert("出現錯誤，請檢查Open API Key，或稍後再嘗試！");
    }
    setIsLoading(false);
  };

  const handleLogout = () => {
    const confirmLogout = window.confirm("是否登出？");
    if (confirmLogout) {
      localStorage.clear();
      // setShowLogoutMenu(false);
      // setIsIntroVisible(true);
      window.location.reload();
    }
  };

  const handleSetAPIKey = () => {
    localStorage.setItem("API_KEY", apiKey);
    setShowAPIKeyModal(false);
    alert("API Key 已設置!");
  };

  const validatePassword = async (password) => {
    const response = await fetch("/verify_password", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ password: password }),
    });

    const data = await response.json();

    if (data.status === "success") {
      localStorage.setItem("login", "yes");
      localStorage.setItem("type", "internal");
      setIsIntroVisible(false);
      setIsPasswordIncorrect(false);
      setShowfirstStep(true);
    } else {
      setIsPasswordIncorrect(true);
    }
  };

  useEffect(() => {
    if (localStorage.getItem("login") === "yes") {
      setIsIntroVisible(false);
    }
  }, []);

  useEffect(() => {
    console.log(prompt);
  }, [prompt]);

  // 創建組合字串的函數
  const combinePrompt = () => {
    // 獲取所有已選中的checkbox項目
    const checkedItems = Object.keys(checkboxValues)
      .filter((key) => checkboxValues[key])
      .join("/");

    // 獲取所有新增的項目
    const addedItemsStr = addedItems.join("/");

    // 使用斜線組合上述兩個字串
    const combinedStr =
      checkedItems + (checkedItems && addedItemsStr ? "/" : "") + addedItemsStr;

    return combinedStr;
  };

  return (
    <div style={styles.mainContainer} onClick={() => setShowLogoutMenu(false)}>
      {isIntroVisible ? (
        <div style={styles.contentContainer}>
          <div
            style={{
              marginBottom: "40px",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <div
              style={{
                fontSize: "42px",
                fontWeight: "bold",
              }}
            >
              歡迎使用「智慧文檔助手 V1.0」
            </div>
          </div>
          <div
            style={{
              borderRadius: "10px",
              boxShadow: "2px 2px 5px rgba(0, 0, 0, 0.2)",
              width: "500px",
              height: "300px",
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
              alignItems: "Start",
              gap: "20px",
              backgroundColor: "white",
              padding: "20px",
            }}
          >
            <div
              style={{
                fontSize: "24px",
                fontWeight: "bold",
              }}
            >
              登入
            </div>
            {showfirstStep ? (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  width: "100%",
                  padding: "0px 60px 10px 60px",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "Column",
                    alignItems: "start",
                    gap: "12px",
                  }}
                >
                  <div style={{ fontSize: "20px", fontWeight: "bold" }}>
                    版本選擇：
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                      gap: "12px",
                    }}
                  >
                    <input
                      type="radio"
                      id="internalUse"
                      value="正式版 (含 API Key，登入後可直接使用)"
                      checked={
                        selectedOption ===
                        "正式版 (含 API Key，登入後可直接使用)"
                      }
                      onChange={(e) => setSelectedOption(e.target.value)}
                      style={{ width: "18px", height: "18px" }}
                    />
                    <label
                      htmlFor="internalUse"
                      style={{
                        fontSize: "18px",
                        fontWeight: "bold",
                        marginRight: "12px",
                      }}
                    >
                      正式版 (含 API Key，登入後可直接使用)
                    </label>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                      gap: "12px",
                    }}
                  >
                    <input
                      type="radio"
                      id="externalUse"
                      value="外部測試版 (需自行設定 API Key)"
                      checked={
                        selectedOption === "外部測試版 (需自行設定 API Key)"
                      }
                      onChange={(e) => setSelectedOption(e.target.value)}
                      style={{ width: "18px", height: "18px" }}
                      disabled={true}
                    />
                    <label
                      htmlFor="externalUse"
                      style={{
                        color: "gray",
                        fontSize: "18px",
                        fontWeight: "bold",
                        marginRight: "12px",
                      }}
                    >
                      外部測試版 (需自行設定 API Key)
                    </label>
                  </div>
                </div>
              </div>
            ) : showEAPasswordBar ? (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  width: "100%",
                  alignItems: "start",
                  justifyContent: "center",
                }}
              >
                <label
                  style={{
                    fontSize: "18px",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  請輸入密碼：
                  <input
                    type="password"
                    onChange={(e) => {
                      setEAPassword(e.target.value);
                      setIsPasswordIncorrect(false); // 當使用者開始重新輸入時，設定此狀態為false
                    }}
                    onKeyDown={(e) => {
                      if (e.key === "Enter") {
                        validatePassword(EAPassword);
                      }
                    }}
                    style={{
                      height: "28px",
                      border: isPasswordIncorrect
                        ? "2px solid red"
                        : "1px solid black", // 如果密碼錯誤，顯示紅色邊框
                    }}
                  />
                </label>
                {isPasswordIncorrect && (
                  <div style={{ color: "red" }}>密碼錯誤</div>
                )}
              </div>
            ) : (
              <div style={{ display: "flex", flexDirection: "column" }}>
                <label style={{ fontSize: "18px" }}>
                  輸入 OpenAI API Key：
                  <input
                    type="text"
                    onChange={(e) => {
                      setApiKey(e.target.value);
                    }}
                    onKeyDown={(e) => {
                      if (e.key === "Enter") {
                        if (apiKey.trim() === "") {
                          alert("尚未輸入 OpenAI API Key");
                          return;
                        }
                        localStorage.setItem("login", "yes");
                        localStorage.setItem("type", "external");
                        localStorage.setItem("API_KEY", apiKey);
                        setIsIntroVisible(false);
                        setShowfirstStep(true);
                      }
                    }}
                    style={{
                      height: "28px",
                      border: "1px solid black",
                    }}
                  />
                </label>
              </div>
            )}
            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "flex-end",
                flexDirection: "row",
                gap: "12px",
              }}
            >
              {!showfirstStep && (
                <button
                  style={styles.button}
                  onClick={() => {
                    setShowfirstStep(true);
                    setEAPassword("");
                    setApiKey("");
                  }}
                >
                  上一步
                </button>
              )}
              {showfirstStep && (
                <button
                  style={styles.button}
                  onClick={() => {
                    setShowfirstStep(false);
                    if (
                      selectedOption === "正式版 (含 API Key，登入後可直接使用)"
                    ) {
                      setShowEAPasswordBar(true);
                    } else {
                      setShowEAPasswordBar(false);
                    }
                  }}
                >
                  下一步
                </button>
              )}
              {!showfirstStep &&
                selectedOption === "正式版 (含 API Key，登入後可直接使用)" && (
                  <button
                    style={styles.button}
                    onClick={() => {
                      validatePassword(EAPassword);
                    }}
                  >
                    登入
                  </button>
                )}
              {!showfirstStep &&
                selectedOption === "外部測試版 (需自行設定 API Key)" && (
                  <button
                    style={styles.button}
                    onClick={() => {
                      if (apiKey.trim() === "") {
                        alert("尚未輸入 OpenAI API Key");
                        return;
                      }
                      localStorage.setItem("login", "yes");
                      localStorage.setItem("type", "external");
                      localStorage.setItem("API_KEY", apiKey);
                      setIsIntroVisible(false);
                      setShowfirstStep(true);
                    }}
                  >
                    設定
                  </button>
                )}
            </div>
          </div>
          <div
            style={{
              fontSize: "16px",
              width: "55%",
              fontStyle: "italic",
              display: "flex",
              flexDirection: "column",
              alignItems: "start",
              gap: "6px",
              marginTop: "80px",
            }}
          >
            <div style={{ fontWeight: "bold" }}>主要功能</div>
            <div>
              《校正文稿》：為使用者提供文稿的校正建議，可以上傳「.pptx」檔案，並可修改校正項目，再透過
              OpenAI 針對文稿中的語法、用詞或其他潛在錯誤，提供修正建議。
            </div>
            <div>
              《彙整摘要》：協助使用者彙整文件的主要內容與重點；可以上傳「.ppt、.doc、.pdf」格式的文件，透過
              OpenAI 提取其主要觀點並產生摘要。
            </div>
          </div>
        </div>
      ) : (
        <>
          <div style={styles.mainContainer}>
            <div style={styles.headerContainer}>
              <div style={styles.title}>智慧文檔助手</div>
              <div
                style={styles.setting}
                onClick={(e) => {
                  e.stopPropagation();
                  setShowLogoutMenu(!showLogoutMenu);
                }}
              >
                <SettingsIcon style={{ width: "50px", height: "50px" }} />
                {showLogoutMenu && (
                  <div
                    style={{
                      position: "absolute",
                      right: "40px",
                      top: "100px",
                      backgroundColor: "white",
                      padding: "10px 20px 10px 20px",
                      borderRadius: "5px",
                      boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
                      zIndex: 1000, // 添加這行
                      fontSize: "35px",
                    }}
                  >
                    {localStorage.getItem("type") === "external" && (
                      <div
                        onClick={() => setShowAPIKeyModal(true)}
                        style={{ cursor: "pointer" }}
                      >
                        設置API Key
                      </div>
                    )}
                    <div onClick={handleLogout} style={{ cursor: "pointer" }}>
                      登出
                    </div>
                  </div>
                )}
                {showAPIKeyModal && (
                  <div style={styles.modal}>
                    <div style={styles.card}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          width: "100%",
                          alignItems: "center",
                        }}
                      >
                        <label style={{ fontSize: "16px" }}>
                          OpenAI API Key：
                          <input
                            type="text"
                            value={apiKey}
                            onChange={(e) => setApiKey(e.target.value)}
                            style={{
                              height: "28px",
                              border: "1px solid black",
                            }}
                          />
                        </label>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          gap: "12px",
                          justifyContent: "flex-end",
                        }}
                      >
                        <div
                          style={styles.buttonContainer}
                          onClick={handleSetAPIKey}
                        >
                          <label
                            htmlFor="fileInput"
                            style={styles.inputFileLabel}
                            onMouseOver={handleMouseOver}
                            onMouseOut={handleMouseOut}
                          >
                            保存
                          </label>
                        </div>
                        <div
                          style={styles.buttonContainer}
                          onClick={() => {
                            setShowAPIKeyModal(false);
                            setShowLogoutMenu(false);
                          }}
                        >
                          <label
                            htmlFor="fileInput"
                            style={styles.inputFileLabel}
                            onMouseOver={handleMouseOver}
                            onMouseOut={handleMouseOut}
                          >
                            取消
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                width: "100%",
              }}
            >
              <div
                style={{
                  padding: "0px 40px 10px 40px",
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "start",
                }}
              >
                <div style={styles.boldText}>功能選擇：</div>
                <input
                  type="radio"
                  id="correction"
                  name="feature"
                  value="校正文稿"
                  checked={feature === "校正文稿"}
                  onChange={handleFeatureChange}
                  style={{
                    width: "18px",
                    height: "18px",
                    cursor: isLoading ? "not-allowed" : "pointer",
                  }}
                  disabled={isLoading} // Disable when isLoading is true
                />
                <label
                  htmlFor="correction"
                  style={{
                    fontSize: "18px",
                    fontWeight: "bold",
                    marginRight: "12px",
                    cursor: isLoading ? "not-allowed" : "pointer",
                  }}
                >
                  校正文稿
                </label>
                <input
                  type="radio"
                  id="summary"
                  name="feature"
                  value="摘要統整"
                  checked={feature === "摘要統整"}
                  onChange={handleFeatureChange}
                  style={{
                    width: "18px",
                    height: "18px",
                    cursor: isLoading ? "not-allowed" : "pointer",
                  }}
                  disabled={isLoading} // Disable when isLoading is true
                />
                <label
                  htmlFor="summary"
                  style={{
                    fontSize: "18px",
                    fontWeight: "bold",
                    cursor: isLoading ? "not-allowed" : "pointer",
                  }}
                >
                  彙整摘要
                </label>
              </div>
            </div>
            {feature === "校正文稿" ? (
              <div style={styles.contentContainerPrompt}>
                <div style={styles.innerContent}>
                  <div
                    style={{
                      fontSize: "16px",
                      fontWeight: "bold",
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <div>選擇校正項目：</div>
                    <input
                      type="text"
                      placeholder="新增校正項目"
                      value={newCorrectionItem}
                      onChange={(e) => setNewCorrectionItem(e.target.value)}
                    />
                    <div style={styles.buttonContainer}>
                      <button
                        onClick={() => {
                          if (newCorrectionItem) {
                            setAddedItems((prev) => [
                              ...prev,
                              newCorrectionItem,
                            ]);
                            setNewCorrectionItem(""); // 清空輸入框
                            const updatedPrompt = combinePrompt();
                            setPrompt(updatedPrompt);
                          }
                        }}
                      >
                        新增
                      </button>
                    </div>
                  </div>
                  <div
                    style={{
                      fontSize: "20px",
                      flex: 1,
                      // backgroundColor: "red",
                      display: "flex",
                      flexDirection: "row",
                      flexWrap: "wrap",
                      maxHeight: "300px",
                      overflowY: "auto",
                    }}
                  >
                    {Object.keys(checkboxValues).map((key) => (
                      <div
                        key={key}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          marginRight: "10px",
                        }}
                      >
                        <input
                          type="checkbox"
                          id={key}
                          checked={checkboxValues[key]}
                          onChange={(e) => {
                            setCheckboxValues((prev) => ({
                              ...prev,
                              [key]: e.target.checked,
                            }));
                            const updatedPrompt = combinePrompt();
                            setPrompt(updatedPrompt);
                          }}
                        />
                        <label htmlFor={key}>{key}</label>
                      </div>
                    ))}
                    {addedItems.map((item) => (
                      <div
                        key={item}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          marginRight: "10px",
                        }}
                      >
                        <span>{item}</span>
                        <button
                          onClick={() => {
                            setAddedItems((prev) =>
                              prev.filter((i) => i !== item)
                            );
                            const updatedPrompt = combinePrompt();
                            setPrompt(updatedPrompt);
                          }}
                        >
                          ✖
                        </button>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            ) : (
              <div></div>
            )}
            <div style={styles.footerContainer}>
              <div style={styles.sectionContainer}>
                <div
                  style={{
                    display: "flex",
                    gap: "12px",
                    alignItems: "center",
                    height: "32px",
                  }}
                >
                  <div style={styles.boldText}>
                    {feature === "校正文稿"
                      ? "上傳文件（.pptx）："
                      : "上傳文件（.pptx, .docx, .pdf）："}
                  </div>
                  <div style={styles.buttonContainer}>
                    <div>
                      <input
                        id="fileInput"
                        accept={
                          feature === "校正文稿"
                            ? ".pptx"
                            : ".pptx, .docx, .pdf"
                        }
                        type="file"
                        style={styles.inputFile}
                        onChange={onFileChange}
                      />
                      {isUploadLoading && (
                        <Comment
                          visible={true}
                          height="150"
                          width="150"
                          ariaLabel="comment-loading"
                          wrapperStyle={styles.commentPosition} // 使用新的style
                          wrapperClass="comment-wrapper"
                          color="#fff"
                          backgroundColor="rgba(0, 0, 0, 0.3)"
                        />
                      )}
                      {!isLoading && (
                        <label
                          htmlFor="fileInput"
                          style={styles.inputFileLabel}
                          onMouseOver={handleMouseOver}
                          onMouseOut={handleMouseOut}
                        >
                          上傳
                        </label>
                      )}
                    </div>
                  </div>
                </div>
                <textarea
                  style={{ ...styles.textArea, userSelect: "text" }}
                  value={feature === "摘要統整" ? textSummary : text}
                  readOnly={true}
                ></textarea>
              </div>
              <div style={styles.centerButtonContainer}>
                <button
                  style={styles.playButton} // 使用新的style
                  onMouseOver={handleMouseOver}
                  onMouseOut={handleMouseOut}
                  onClick={() => {
                    console.log(prompt);
                    if (isLoading) return;
                    if (localStorage.getItem("type") === "external") {
                      if (localStorage.getItem("API_KEY") === null) {
                        alert("尚未設置 OpenAI API Key");
                        return;
                      } else if (
                        localStorage.getItem("API_KEY").trim() === ""
                      ) {
                        alert("尚未設置 OpenAI API Key");
                        return;
                      } else {
                        if (feature === "摘要統整") {
                          handleSubmitSummary(
                            text,
                            localStorage.getItem("API_KEY")
                          );
                        } else {
                          handleSubmit(
                            prompt,
                            text,
                            localStorage.getItem("API_KEY")
                          );
                        }
                      }
                    } else {
                      if (feature === "摘要統整") {
                        handleSubmitSummary(text);
                      } else {
                        handleSubmit(prompt, text);
                      }
                    }
                  }}
                  disabled={isLoading} // Disable when isLoading is true
                >
                  <div style={styles.playIcon}></div> {/* 播放圖標 */}
                </button>
                <div>詢問 OpenAI</div>
              </div>
              <div style={styles.sectionContainer}>
                <div style={styles.boldText}>OpenAI 回覆結果：</div>
                {isLoading && (
                  <Comment
                    visible={true}
                    height="150"
                    width="150"
                    ariaLabel="comment-loading"
                    wrapperStyle={styles.commentPosition} // 使用新的style
                    wrapperClass="comment-wrapper"
                    color="#fff"
                    backgroundColor="rgba(0, 0, 0, 0.3)"
                  />
                )}
                <textarea
                  style={{ ...styles.textArea, userSelect: "text" }}
                  value={feature === "摘要統整" ? answerSummary : answer}
                  readOnly={true}
                ></textarea>
              </div>
            </div>
          </div>
        </>
      )}
    </div>
  );
}

export default App;
