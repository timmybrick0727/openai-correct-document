import openai
import time
import re
import yaml
import docx
import PyPDF2
from io import BytesIO
from flask import Flask, request, jsonify
from flask_cors import CORS
from yaml import Dumper, SafeDumper
from pptx import Presentation
from collections import OrderedDict


######################################################################## 基本設定
openai.api_type = "azure"
openai.api_base = "https://eaaasgpt4turbo.openai.azure.com/"
openai.api_version = "2023-05-15"
DEFAULT_API_KEY = "1df05334a7054ad9b4f4f24a4ff074e3"
engine = "EAUSERGPT432K"
temperature = 0.5
max_tokens = 4000
frequency_penalty = 0.5
timeout = 10
sleep_time = 2
base_prompt = "以下YAML為一個pptx的內容，參考離PPT頁面左邊、上邊距離，對「內容」進行（1.拼寫檢查 2.語法檢查 3.標點符號必須為全形 4.檢查是否出現中文簡體、英文字詞全部大寫 5.檢查所有頁面所有文字及表格）；請用繁體中文條列需要修正之處「哪一頁：建議是什麼？修改前、修改後成什麼樣子？」"

# app = Flask(__name__)
app = Flask(__name__, static_folder="build", static_url_path="/")
app.config['MAX_CONTENT_LENGTH'] = 1024 * 1024 * 1024  # 1GB
# app.config["MAX_CONTENT_LENGTH"] = 16 * 1024 * 1024  # 設置為16MB
CORS(app)


######################################################################## OpenAI相關function
def clean_text(text):
    text = re.sub("<.*?>", "", text)
    text = re.sub("&nbsp;", " ", text)
    text = re.sub("&amp;", "&", text)
    text = re.sub("&gt;", ">", text)
    text = re.sub("&lt;", "<", text)
    return text


def get_answer(messages):
    result = clean_text(
        openai.ChatCompletion.create(
            engine=engine,
            messages=messages,
            temperature=temperature,
            max_tokens=max_tokens,
            top_p=0.95,
            frequency_penalty=frequency_penalty,
            presence_penalty=0,
            stop=None,
        )["choices"][0]["message"]["content"]
    )
    time.sleep(sleep_time)
    return result


def display_answer(task, data):
    return get_answer(
        [
            {
                "role": "user",
                "content": f"以下YAML為一個pptx的內容，參考離PPT頁面左邊、上邊距離，對「內容」進行（{task}）；請用繁體中文條列需要修正之處「哪一頁：建議是什麼？修改前、修改後成什麼樣子？」\n\n{data}",
            }
        ]
    )


def display_answer_summary(data):
    return get_answer(
        [
            {
                "role": "user",
                "content": f"將以下內容，彙整摘要\n\n{data}",
            }
        ]
    )


######################################################################## 檔案處理function
def extract_docx(file):
    """提取.docx文件的內容"""
    doc = docx.Document(file)
    fullText = []
    for para in doc.paragraphs:
        fullText.append(para.text)
    return "\n".join(fullText)


def extract_pdf(file_obj):
    """提取.pdf文件的內容"""
    text = ""
    try:
        pdf = PyPDF2.PdfReader(file_obj)
        for page in pdf.pages:
            text += page.extract_text()
    except Exception as e:
        print(f"Error extracting text from PDF: {e}")
    return text


def represent_ordereddict(dumper, data):
    return dumper.represent_mapping("tag:yaml.org,2002:map", data.items())


Dumper.add_representer(OrderedDict, represent_ordereddict)
SafeDumper.add_representer(OrderedDict, represent_ordereddict)


def load_ppt(file_path):
    prs = Presentation(file_path)
    result = OrderedDict()
    for index, slide in enumerate(prs.slides):
        titles = []
        slide_content = OrderedDict([("標題", ""), ("主文章", [])])
        for shape in slide.shapes:
            if hasattr(shape, "text") and shape.text.strip() != "":
                modified_text = " ".join(
                    [para.text for para in shape.text_frame.paragraphs]
                ).replace(
                    "\n", " "
                )  # 將 '\n' 替換為空格
                font_sizes = []
                for paragraph in shape.text_frame.paragraphs:
                    for run in paragraph.runs:
                        if run.font.size:
                            font_sizes.append(run.font.size.pt)
                min_font_size = min(font_sizes) if font_sizes else None
                if (shape.top <= 850000) or (
                    min_font_size is not None and min_font_size >= 40
                ):
                    titles.append(modified_text)
                elif shape.top > 850000 and shape.top <= 6400000:
                    content_dict = OrderedDict(
                        [
                            ("種類", "文字"),
                            ("內容", modified_text),
                        ]
                    )
                    slide_content["主文章"].append(content_dict)
            # elif hasattr(shape, "table"):
        try:
            if hasattr(shape, "table") and shape.table is not None:
                table_content = []
                for row in shape.table.rows:
                    table_row = [
                        " ".join(
                            [para.text for para in cell.text_frame.paragraphs]
                        ).replace("\n", " ")
                        for cell in row.cells
                    ]
                    table_content.append(table_row)
                content_dict = OrderedDict(
                    [
                        ("種類", "表格"),
                        ("內容", table_content),
                    ]
                )
                slide_content["主文章"].append(content_dict)
        except ValueError:
            print("upload error")
        if titles:
            slide_content["標題"] = "，".join(titles)
        else:
            slide_content["標題"] = "未有標題"
        result[f"第{index + 1}頁"] = slide_content
    return yaml.dump(result, Dumper=yaml.Dumper, allow_unicode=True)


######################################################################## Flask API
@app.route("/")
def serve():
    return app.send_static_file("index.html")


@app.route("/upload", methods=["POST"])
def upload():
    file = request.files.get("file")
    if file:
        if file.filename.endswith(".pptx"):
            return load_ppt(file), 200
        elif file.filename.endswith(".docx"):
            text = extract_docx(file)
            return text, 200
        elif file.filename.endswith(".pdf"):
            text = extract_pdf(BytesIO(file.read()))
            return text, 200
        else:
            return jsonify({"error": "Unsupported file format"}), 400
    return jsonify({"error": "No file provided"}), 400


@app.route("/get_answer", methods=["POST"])
def get_display_answer():
    task = request.json.get("task")
    data = request.json.get("data")
    api_key = request.json.get(
        "api_key", DEFAULT_API_KEY
    )  # Get the API Key from POST request, if not provided, use default

    if not api_key or api_key.strip() == "":
        return (
            jsonify({"error": "API Key not provided and default key is not set"}),
            400,
        )

    openai.api_key = api_key  # Set the API Key for OpenAI

    if task and data:
        answer = display_answer(task, data)
        return jsonify({"answer": answer}), 200
    return jsonify({"error": "Mission or Data not provided"}), 400


@app.route("/get_answer_summary", methods=["POST"])
def get_display_answer_summary():
    data = request.json.get("data")
    api_key = request.json.get(
        "api_key", DEFAULT_API_KEY
    )  # Get the API Key from POST request, if not provided, use default

    if not api_key or api_key.strip() == "":
        return (
            jsonify({"error": "API Key not provided and default key is not set"}),
            400,
        )

    openai.api_key = api_key  # Set the API Key for OpenAI

    if data:
        answer = display_answer_summary(data)
        return jsonify({"answer": answer}), 200
    return jsonify({"error": "Mission or Data not provided"}), 400


@app.route("/verify_password", methods=["POST"])
def verify_password():
    password = request.json.get("password")

    if password == "EA2023":
        return jsonify({"status": "success"}), 200
    return jsonify({"status": "fail"}), 400


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5050)
